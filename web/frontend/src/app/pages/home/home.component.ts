import {Component, OnInit} from '@angular/core';
import {AmiService} from "./ami.service";

import {
    inParameters,
    buildResult
} from "./types";

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    public isBuilding: boolean = false;

    public tooltipHidden: boolean = true;
    toggleHidden() {this.tooltipHidden = !this.tooltipHidden}

    public tooltipHidden2: boolean = true;
    toggleHidden2() {this.tooltipHidden2 = !this.tooltipHidden2}

    public inParameters: inParameters = new inParameters();
    public buildResult: buildResult;



    // graph styling
    public colorScheme = {
        domain: ['#1a242c', '#e81746', '#e67303', '#f0f0f0']
    };

    constructor(private amiService: AmiService) {
    }

    ngOnInit() {
    }

    public predict() {
        this.isBuilding = true;
        this.amiService.predict(this.inParameters).subscribe((newcases) => {
            this.buildResult = newcases;
            this.isBuilding = false;
        });
    }

//-------------------------------------------------
//    Pre-populating the first form
//    (@TODO: how to make this smarter?)
//    (@TODO: fill all fields)
//-------------------------------------------------
    public setFrance() {
        //this.inParameters.xx_1=true;
        this.inParameters.xx_2='FRA';
        this.inParameters.xx_3='209.515454309848';  //IND_CO2_MED
        this.inParameters.xx_4='18/04/20#31/07/20'; //Awareness campaigns
        this.inParameters.xx_5='00/00/00#00/00/00';
        this.inParameters.xx_6='17/03/00#31/07/20';//Health screenings in airports and border crossings
        this.inParameters.xx_7='17/03/00#31/07/20'; //International flights suspension
        this.inParameters.xx_8='30/03/20#31/07/20'; //Lockdown
        this.inParameters.xx_9='17/03/20#31/07/20';   //military deployment
        this.inParameters.xx_10='17/03/20#31/07/20'; //schools closure
        this.inParameters.xx_11='25/03/20#31/07/20';  //state of emergency declared
        this.inParameters.xx_12='25/03/20#31/07/20'; //Strengthening the public health
        this.inParameters.xx_13='00/00/00#00/00/00'; //Surveillance
        this.inParameters.xx_14='25/04/20#31/07/20';   //testing policy
        this.inParameters.xx_15='25/04/20#31/07/20';  //visa restrictions
        this.inParameters.xx_16='82.66';
        this.inParameters.xx_17='65273512';

        this.inParameters.xx_1=false;
        this.toggleHidden();
    }


    public setGermany() {
        //this.inParameters.xx_1=true;
        this.inParameters.xx_2='DEU';
        this.inParameters.xx_3='343.30221620181';  //IND_CO2_MED
        this.inParameters.xx_4='07/02/20#31/07/20'; //Awareness campaigns
        this.inParameters.xx_5='00/00/00#00/00/00';
        this.inParameters.xx_6='00/00/00#00/00/00';//Health screenings in airports and border crossings
        this.inParameters.xx_7='00/00/00#00/00/00'; //International flights suspension
        this.inParameters.xx_8='00/00/00#00/00/00'; //Lockdown
        this.inParameters.xx_9='04/04/20#31/07/20';   //military deployment
        this.inParameters.xx_10='17/03/20#31/07/20'; //schools closure
        this.inParameters.xx_11='17/03/20#31/07/20';  //state of emergency declared
        this.inParameters.xx_12='12/03/20#31/07/20'; //Strengthening the public health
        this.inParameters.xx_13='08/04/20#31/07/20'; //Surveillance
        this.inParameters.xx_14='30/04/20#31/07/20';   //testing policy
        this.inParameters.xx_15='10/04/20#31/07/20';  //visa restrictions
        this.inParameters.xx_16='81.33';
        this.inParameters.xx_17='83783945';

        this.inParameters.xx_1=false;
        this.toggleHidden();
    }

}
