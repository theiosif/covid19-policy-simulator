import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {
    buildResult,
    inParameters
} from "./types";

const SERVER_URL: string = 'api/';

@Injectable()
export class AmiService {

    constructor(private http: Http) {
    }

    public predict(inParameters: inParameters): Observable<buildResult> {
        return this.http.post(`${SERVER_URL}predict`, inParameters).map((res) => res.json());
    }

}
