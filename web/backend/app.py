from flask import Flask, request, jsonify
import sklearn
import time
import sys
import ami
import os
import pandas as pd
import numpy as np

# declare constants
HOST = '0.0.0.0'
PORT = 8081

# initialize flask application
app = Flask(__name__)

# The classics
DEBUG = True


def dbg_print(what):
    """
    prints to stderr ( in flask log)
    input: LIST of anything
    """
    if DEBUG:
        print("[!!]>>>[DEBUG]: ", what, file=sys.stderr)


@app.route('/api/predict', methods=['POST'])
def build():

    starting_dir = os.path.dirname(os.path.realpath(__file__))
    os.chdir(starting_dir)
    os.chdir("models")
    model_dir = os.getcwd()

    dbg_print(model_dir)

    # get parameters from request
    parameters = request.get_json()

    # renaming parameters to match names of dataset
    features = ['own', 'ISO_Code', 'IND_CO2_MED', 'Awareness campaigns', 'Full lockdown', 'Health screenings in airports and border crossings',
             'International flights suspension', 'Lockdown of refugee/idp camps or other minorities', 'Military deployment', 'Schools closure',
             'State of emergency declared', 'Strengthening the public health system', 'Surveillance and monitoring', 'Testing policy',
             'Visa restrictions', 'life_expectancy', 'population']
    params = ami.rename(parameters, features)

    # typecast parameters to int
    params['IND_CO2_MED'] = float(params['IND_CO2_MED'])
    params['life_expectancy'] = float(params['life_expectancy'])
    params['population'] = int(params['population'])

    dbg_print((params.values()))

    # convert parameters to dataframe
    df = ami.paramtoDF(params, model_dir)

    # predict
    y_cases = ami.prediction(df, "cases", model_dir)
    y_gdp = ami.prediction(df, "GDP", model_dir)
    y_co2 = ami.prediction(df, "co2", model_dir)

    # check if own country
    if not params['own']:
        df = pd.read_csv(model_dir + "/FINAL_DATASET.csv")
        country = params['ISO_Code']
        df = df.loc[df['ISO_Code'] == country]

        y_known_cases = df['new_cases_per_million']
        y_known_co2 = df['TOTAL_CO2_MED']
        y_known_gdp = df['GDP']

        y_known_gdp = list(y_known_gdp)
        y_known_co2 = list(y_known_co2)
        y_known_cases = list(y_known_cases)

        y_cases = y_known_cases + y_cases
        y_gdp = y_known_gdp + y_gdp
        y_co2 = y_known_co2 + y_co2

    # summarizing stats to one number for front end
    y_cases = np.sum(y_cases)
    y_co2 = np.mean(y_co2) * 100
    y_gdp = y_gdp[-1]

    dbg_print(y_cases)
    dbg_print(y_co2)
    dbg_print(y_gdp)

    return jsonify({'yy_1': y_cases, 'yy_2': y_co2, 'yy_3': y_gdp})


if __name__ == '__main__':
    # run web server
    app.run(host=HOST,
            debug=True,  # automatic reloading enabled
            port=PORT)
