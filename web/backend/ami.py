from datetime import datetime as dt
import numpy as np
import pandas as pd
import pickle
import os
import sys


def rename(parameters, names):

    params = dict.fromkeys(names)
    values = list(parameters.values())

    i = 0

    for key in params.keys():
        params[key] = values[i]
        i = i + 1

    return params


def paramtoDF(params, model_dir):
    """
    DAY -> generate on own
    CO2 -> use mean from pickled model and ???
    binary inputs: use start and end date
    -> Awareness campaign
    -> Full Lockdown
    -> Health screenings
    -> International Flight suspension
    -> Lockdown of refugee
    -> Military deployment
    -> Schools closure
    -> State of emergency
    -> Strengething public health
    -> Surveillance
    -> Testing policy
    -> Visa restrictions
    Life expectancy -> write value x times
    population -> write value x times

    :param params: List of parameters the user enters
    :param model_dir: directory where models and dataset is located
    :return: df: Dataframe created with parameters
    """

    # CO2 mean and variance
    co2_mean = -0.007043818143527788
    co2_var = 0.00016437361222961484

    df = pd.DataFrame()

    # if loaded country only predict everything after 31.07.
    today = dt.now()
    today = int(today.strftime('%j'))

    own_country = params['own']

    if own_country:
        start = 0
    else:
        start = 213

    days = np.arange(start + 1, today + 1, 1)
    df['DAY'] = days

    for key, value in params.items():
        # print(key)
        if key in ['own', 'ISO_Code']:
            continue
        if key not in (['IND_CO2_MED', 'life_expectancy', 'population']):  # is not CO2, life, population
            if own_country:
                start_date, end_date = value.split('#')
                start_day = str2day(start_date)
                end_day = str2day(end_date)
                values = np.zeros(today - start)
                values[start_day - 1:end_day - 1] = 1
            else:
                values = np.ones(today - start) * np.nan
        elif key == 'IND_CO2_MED':  # check if CO2 -> do mean stuff
            values = np.ones(today - start) * co2_mean * ((value + 100) / 100)
            co2_vars = np.random.random((today-start, 1)) * co2_var
            co2_vars = co2_vars[:, 0]
            values = values + co2_vars
        else:  # life and population
            values = np.ones(today - start) * value

        # print(values)
        df[key] = values

    # fill nans
    if not own_country:
        df1 = pd.read_csv(model_dir + "/FINAL_DATASET.csv")
        country = params['ISO_Code']
        df1 = df1.loc[df1['ISO_Code'] == country]
        length = len(df1)
        cols = list(params.keys())
        cols = cols[1:]
        cols[0] = 'DAY'
        df1 = df1[cols]
        df = pd.concat([df1, df], axis=0)

        try:
            df.drop(columns=['ISO_Code'], inplace=True)
        except:
            print('No ISO Code in here')

        df.reset_index(inplace=True, drop=True)
        df.fillna(method='ffill', axis=0, inplace=True)
        mask = np.arange(0, length, 1)
        df.drop(mask, inplace=True)
        df.reset_index(inplace=True, drop=True)

    return df


def str2day(date):

    date = dt.strptime(date, '%d/%m/%y')
    day = int(date.strftime('%j'))
    return day


def prediction(df, model_name, model_dir):

    """
    # navigate to models directory
    if model_name == "cases":
        os.chdir(model_dir + "/Covid_Cases")
    elif model_name == "co2":
        os.chdir(model_dir + "/CO2_Emissions_Prediction")
    elif model_name == "GDP":
        os.chdir(model_dir + "/GDP_Prediction")
    """

    # load model
    pkl_filename = "/random_forest_model_" + model_name + ".pkl"
    with open(model_dir + pkl_filename, 'rb') as file:
        loaded_model = pickle.load(file)

    keys = ['IND_CO2_MED', 'life_expectancy', 'population']

    # select features based on model
    if model_name == "co2":
        df.drop(columns=["IND_CO2_MED"], inplace=True)
        keys.remove('IND_CO2_MED')

    # normalize data
    for col in keys:  # df.columns[1:]:
        df[col] = (df[col] - loaded_model.mean_vector[col]) / loaded_model.variance_vector[col]

    y_out = list()

    # predict one new value at a time
    for i in range(len(df)):
        row = df.loc[df.index == i]
        y = loaded_model.predict(row)
        y = y[0][0]
        y_out.append(y)

    return y_out


def dbg_print(what):
    """
    prints to stderr ( in flask log)
    input: LIST of anything
    """
    print("[!!]>>>[DEBUG]: ", what, file=sys.stderr)


def dbg_print_mult(what):
    """
    prints to stderr ( in flask log)
    input: LIST of anything
    """
    for wtf in what:
        print("[!!]>>>[DEBUG]: ", wtf, file=sys.stderr)
