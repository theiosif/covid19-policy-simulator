import numpy as np
import pandas as pd
import datetime
#import matplotlib.pyplot as plt
dataset = 'venv/Summary_stats_all_locs.csv'
df = pd.read_csv(dataset)
columns = ['location_name','all_bed_capacity','icu_bed_capacity','travel_limit_start_date','travel_limit_end_date','stay_home_start_date','stay_home_end_date','educational_fac_start_date','educational_fac_end_date','all_non-ess_business_start_date','all_non-ess_business_end_date']
columns_dates = ['travel_limit_start_date','travel_limit_end_date','stay_home_start_date','stay_home_end_date','educational_fac_start_date','educational_fac_end_date','all_non-ess_business_start_date','all_non-ess_business_end_date']

df_plus = df[columns]


# for col in df_plus.columns:
#     df_plus[col].replace(['None', 'NaN', 'nan'], np.nan, inplace=True)
#
#
# specify the dtype "objects" as datatime
for text in (columns_dates):
    df_plus[text] = pd.to_datetime(df_plus[text], format='%Y-%m-%d')
df_plus.info()
#
# df_plus['all_non-ess_business_closure_period'] = df_plus['all_non-ess_business_end_date'] - df_plus['all_non-ess_business_start_date']
# df_plus['all_non-ess_business_closure_period'] = df_plus['all_non-ess_business_closure_period'] / np.timedelta64(1, 'D')

#df_plus_period = df_plus[['location_name','all_bed_capacity','icu_bed_capacity','travel_limit_period','stay_home_period','period_educ','all_non-ess_business_closure_period']]

# try to delete all rows with nan as bed_capacity ( also ICU)
rows = []
for i in range(len(df_plus)):
    temp = df_plus.loc[[i]]

    if ( np.isnan(temp._values[0][1]) == False ) & (np.isnan(temp._values[0][2]) == False ):
        rows.append(i)
df_plus_cleaned = df_plus.loc[rows]


# try to compute the period when there is non end_date
df_plus_cleaned['educational_fac_period']= np.nan
df_plus_cleaned['travel_limit_period']= np.nan
df_plus_cleaned['all_non_ess_business_period']= np.nan

df_plus_cleaned['all_non_ess_business_start_date'] = df_plus_cleaned['all_non-ess_business_start_date']
df_plus_cleaned['all_non_ess_business_end_date'] = df_plus_cleaned['all_non-ess_business_end_date']

for i in rows:

    # stay_home_period
    if (pd.isnull(df_plus_cleaned.stay_home_start_date[i])==True):
        df_plus_cleaned.stay_home_period[i] = np.nan
    if (pd.isnull(df_plus_cleaned.stay_home_start_date[i])==False):
        if (pd.isnull(df_plus_cleaned.stay_home_end_date[i])== True):
            x = (datetime.datetime.today() - df_plus_cleaned.stay_home_start_date[i])
            df_plus_cleaned.stay_home_period[i] = x / np.timedelta64(1, 'D')
        else:
             df_plus_cleaned.stay_home_period[i]  = df_plus_cleaned.stay_home_end_date[i] - df_plus_cleaned.stay_home_start_date[i]
             df_plus_cleaned.stay_home_period[i]  = df_plus_cleaned.stay_home_period[i] / np.timedelta64(1, 'D')
    # educational_fac_period
    if (pd.isnull(df_plus_cleaned.educational_fac_start_date[i])==True):
        df_plus_cleaned.educational_fac_period[i] = np.nan
    if (pd.isnull(df_plus_cleaned.educational_fac_start_date[i])==False):
        if (pd.isnull(df_plus_cleaned.educational_fac_end_date[i])== True):
            x = (datetime.datetime.today() - df_plus_cleaned.educational_fac_start_date[i])
            df_plus_cleaned.educational_fac_period[i] = x / np.timedelta64(1, 'D')
        else:
             df_plus_cleaned.educational_fac_period[i]  = df_plus_cleaned.educational_fac_end_date[i] - df_plus_cleaned.educational_fac_start_date[i]
             df_plus_cleaned.educational_fac_period[i]  = df_plus_cleaned.educational_fac_period[i] / np.timedelta64(1, 'D')
    # travel_limit_period
    if (pd.isnull(df_plus_cleaned.travel_limit_start_date[i])==True):
        df_plus_cleaned.travel_limit_period[i] = np.nan
    if (pd.isnull(df_plus_cleaned.travel_limit_start_date[i])==False):
        if (pd.isnull(df_plus_cleaned.travel_limit_end_date[i])== True):
            x = (datetime.datetime.today() - df_plus_cleaned.travel_limit_start_date[i])
            df_plus_cleaned.travel_limit_period[i] = x / np.timedelta64(1, 'D')
        else:
             df_plus_cleaned.travel_limit_period[i]  = df_plus_cleaned.travel_limit_end_date[i] - df_plus_cleaned.travel_limit_start_date[i]
             df_plus_cleaned.travel_limit_period[i]  = df_plus_cleaned.travel_limit_period[i] / np.timedelta64(1, 'D')
    # all_non_ess_business_period
    if (pd.isnull(df_plus_cleaned.all_non_ess_business_start_date[i])==True):
        df_plus_cleaned.all_non_ess_business_period[i] = np.nan
    if (pd.isnull(df_plus_cleaned.all_non_ess_business_start_date[i])==False):
        if (pd.isnull(df_plus_cleaned.travel_limit_end_date[i])== True):
            x = (datetime.datetime.today() - df_plus_cleaned.all_non_ess_business_start_date[i])
            df_plus_cleaned.all_non_ess_business_period[i] = x / np.timedelta64(1, 'D')
        else:
             df_plus_cleaned.all_non_ess_business_period[i]  = df_plus_cleaned.all_non_ess_business_end_date[i] - df_plus_cleaned.all_non_ess_business_start_date[i]
             df_plus_cleaned.all_non_ess_business_period[i]  = df_plus_cleaned.all_non_ess_business_period[i] / np.timedelta64(1, 'D')

columns_final = ['location_name','all_bed_capacity','icu_bed_capacity','travel_limit_period','stay_home_period','educational_fac_period','all_non_ess_business_period']
df_plus_cleaned_final = df_plus_cleaned[columns_final]

df_plus_cleaned_final.to_csv('Summary_stats_all_locs_cleaned.csv',index=False)