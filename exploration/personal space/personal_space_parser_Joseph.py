# In[personal space]
import pandas as pd
import pycountry

NORM = True   # Debug flag, so that I can switch between norm'd
              # And un-normalized data on the fly between blocks

EXPORT = True

pspace = pd.read_csv("../../data/raw_data/personal_space.csv", header=None)
pspace.rename(columns={0:'location', 1:'continent', 2:'pspace_stranger', 3:'pspace_acquaintance', 4:'pspace_intimate'}, inplace=True)
pspace.drop('continent', axis=1, inplace=True)
work_cols = list(pspace.columns)[1:4]
for col in work_cols:
	pspace[col+"_std"] = pspace[col].apply(lambda x: float(x.split(" ")[1][1:-1]))
	pspace[col] = pspace[col].apply(lambda x: float(x.split(" ")[0]))
	pspace[col+"_min"] = pspace[col]-pspace[col+"_std"]
	pspace[col+"_max"] = pspace[col]+pspace[col+"_std"]

# keep just min/max personal space values
pspace.drop(work_cols+[x+"_std" for x in work_cols], axis=1, inplace=True)

def normalize(pdSeries):
    return (pdSeries - pdSeries.mean()) / pdSeries.std()

pspace_norm = pspace.copy()
for col in list(pspace.columns[1:]):
    pspace_norm[col] = normalize(pspace_norm[col])

if NORM:
    pspace = pspace_norm

## Didn't have ISO codes oops
## Little addendum: the 'two koreas' situation confuses the fuzzy_search. manual measures had to be taken
pspace.replace(to_replace='South Korea', value='Republic of Korea', inplace=True)
pspace['iso_code'] = pspace['location'].apply(lambda x: pycountry.countries.search_fuzzy(x)[0].alpha_3 if (x!='Republic of Korea') else 'KOR')
pspace.replace(to_replace='Republic of Korea', value='South Korea', inplace=True)
pspace.to_csv (r'../../data/preprocessed_data/personal_space.csv')

# Are all countries named the same between datasets?
# owid = pd.read_csv("owid-covid-data.csv")
# len(set(owid.location).intersection(set(pspace.location)))==len(set(pspace.location))
# set(pspace.location) - set(owid.location).intersection(set(pspace.location))

# # Fix and check again
# pspace.location.replace(to_replace="USA", value="United States", inplace=True)
# len(set(owid.location).intersection(set(pspace.location)))==len(set(pspace.location))

# #check to see result
# merged = owid.join(pspace.set_index('location'), on='location')
# merged[merged.location=='Argentina'].head()

if EXPORT==True:
	pspace.to_csv(r'../../data/preprocessed_data/personal_space.csv')
