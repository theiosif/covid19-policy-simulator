#In[imports_misc]:
import numpy as np
import pandas as pd
#import seaborn as sns
#from sklearn import preprocessing
#import matplotlib.pyplot as plt
#from IPython.display import display, Markdown

# Would've been cool if this worked, but it doesn't
#def printmd(string, color=None):
#    colorstr = "<span style='color:{}'>{}</span>".format(color, string)
#    display(Markdown(string))


# ------- DEBUG / UTILITY FLAGS ------- #

FINAL = False # Flag to set final dataset columns/countries to be used
countries_final = []
cols_final = []

DEBUG = False  # Debug flag, to comment out blocks of done code.
              # with an "if DONE:"


NORM = True   # Debug flag, so that I can switch between norm'd
              # And un-normalized data on the fly between blocks

LOCAL = not True  # get data from local csv or from web

EXPORT = True  # get data from local csv or from web

# --------------------------------  DATA  ---------------------------------- #
# Legend of dataset columns:
# https://github.com/owid/covid-19-data/blob/master/public/data/owid-covid-data-codebook.md
if LOCAL:
    owid = pd.read_csv("owid-covid-data.csv")
else:
    owid =  pd.read_csv("https://covid.ourworldindata.org/data/owid-covid-data.csv")

# In[intro]:
owid.iso_code = owid.iso_code.fillna('OWID_INT')
owid.continent = owid.continent.fillna(owid.iso_code)


owid = owid.rename(columns={'iso_code': 'ISO_Code', 'date':'Date', 'location':'Country'})
owid_cols = list(owid.columns)
#pspace_cols = list(pspace.columns)

for col in owid.columns:
    owid[col].replace(['None', 'NaN', 'nan'], np.nan, inplace=True)

if DEBUG:
    owid_countries = set(owid['Country'])
    print("Columns:" + str(owid_cols))

if FINAL:
    owid.loc[owid['Country'].isin(countries_final)]
    owid = owid.dropna(subset=list(set(owid_cols)-set(cols_final)))
else:
    if DEBUG:
        mini=owid[owid.ISO_Code.isin(["LTU", "EST", "ETH"])]
        one=owid[owid.ISO_Code=="LTU"]
        print ("\n---\ndropped: " + str( owid_countries -  set(owid['Country']) ) + "\n\n")
        print ("kept: " + str(owid_countries.intersection(set(owid['Country']))) + "\n\n")
        print("final df.shape:" + str(owid.shape))

# In[Date-related]:
owid['Date'] = pd.to_datetime(owid["Date"],format = "%Y-%m-%d")
owid.insert(column='year', loc=4, value=owid.Date.dt.strftime('%Y') )
owid.insert(column='year_week', loc=5, value=owid.Date.dt.week)
owid.insert(column='year_day', loc=6, value=owid.Date.dt.strftime('%j') )


# In[latent-vars]:
owid["new_cases_increase"]= np.where(owid.new_cases != 0, owid.new_cases / owid.total_cases, 0)
owid["death_rate"] = np.where(owid.total_deaths !=0, owid.total_deaths / owid.total_cases, 0)
owid['infect_rate'] = owid.total_cases / owid.population * 100

owid_cols = list(owid.columns)


# In[nan-part1]:
work_cols = ['total_tests',
 'new_tests',
 'total_tests_per_thousand',
 'new_tests_per_thousand',
 'new_tests_smoothed',
 'new_tests_smoothed_per_thousand',
 'cardiovasc_death_rate',
 'diabetes_prevalence',
 'female_smokers',
 'male_smokers',
 'handwashing_facilities',
 'hospital_beds_per_thousand']

def nan_fix(series):
    if series.isna().all():
        return series
    elif pd.isna(series.iloc[0]):
        series.iloc[0] = 0
        series = series.ffill().bfill()
    return series

owid[work_cols] = owid.groupby('Country')[work_cols].transform(nan_fix)


# In[nan-part2]:
owid['median_age'] = owid['median_age'].fillna(owid.median_age.median())
owid['aged_70_older'] = owid['aged_70_older'].fillna(owid.aged_70_older.median())
owid['aged_65_older'] = owid['aged_65_older'].fillna(owid.aged_65_older.median())


# In[nan-report]:
work_cols = owid_cols.copy()

#print("---> NaN's per column: <---\n" + str(owid.isna().sum()))
#print ("\n\n\n")

#print("---> NaNs as percent per country data: <---")
nulls_percent = owid.groupby('Country').apply(lambda x: 1 - x.notnull().mean()).drop('Country', axis=1)
#display(nulls_percent)

work_cols.remove('Country')

df_countries = owid.groupby('Country').aggregate(lambda x: x.tolist())
for col in work_cols[10:]:
	df_countries[col] = df_countries[col].apply(lambda x: pd.Series(x).isnull().all())

#display(" ---> All-NaNs per country per indicator: <---")
#display("ignoring columns where no NaNs were observed)")
#display(df_countries.drop(columns=work_cols[:10]+['year', 'year_day', 'year_week']))



# In[drop_1]:
keepers = list(df_countries[df_countries.stringency_index==False].index)
owid = owid[owid.Country.isin(keepers)]

#print("---> NaN's per column, after stringency_index drop: <---\n" + str(owid.isna().sum()))
#print ("\n\n\n")

#print("We are left with the following countries:\n"), \
#display(set(owid[owid.Country.isin(keepers)].Country))


# In[normalize]:
number_cols = owid.dtypes.apply(str)
number_cols = list(number_cols[number_cols.isin(['float64', 'int64'])].index)[1:]

def normalize(pdSeries):
    return (pdSeries - pdSeries.mean()) / pdSeries.std()

owid_norm = owid.copy()
for col in number_cols:
    owid_norm[col] = normalize(owid_norm[col])

if NORM:
    owid = owid_norm

# Pre-processing for Final Merge
owid = owid.drop(columns=['Country'])
"""
work_cols = ['total_tests',
 'new_tests',
 'total_tests_per_thousand',
 'new_tests_per_thousand',
 'new_tests_smoothed',
 'new_tests_smoothed_per_thousand',
 'cardiovasc_death_rate',
 'diabetes_prevalence',
 'female_smokers',
 'male_smokers',
 'handwashing_facilities',
 'hospital_beds_per_thousand','tests_per_case','positive_rate','tests_units']
# Fill the nan values with 0
for cols in work_cols:
    owid[cols] = owid[cols].fillna(0)
# In[ Cut it down to intended date range]

start_date, end_date = "2020-01-01", "2020-07-31"
Filter = (owid['Date'] >= start_date) & (owid['Date'] <= end_date)
owid = owid.loc[Filter]
"""

start_date, end_date = "2020-01-01", "2020-07-31"

def interpolate(df):
    date_range = pd.date_range(start_date, end_date)

    date_range = date_range.to_frame(index=False, name="Date")

    df = df.set_index("Date").join(date_range.set_index("Date"), how="outer")

    df = df.interpolate(method='linear')
    df.fillna(method='bfill', inplace=True)
    df.fillna(method='ffill', inplace=True)

    return df.reset_index()

# interpolate
owid = owid.sort_values(["ISO_Code", "Date"]).groupby(["ISO_Code"]).apply(interpolate).reset_index(drop=True)

def correct_dates(df):
    date_range = pd.date_range(start_date , end_date)
    
    date_range = date_range.to_frame(index=False, name="Date")

    df = df.set_index("Date").join(date_range.set_index("Date"), how="inner")
    
    return df.reset_index()

owid = owid.groupby(["ISO_Code"]).apply(correct_dates).reset_index(drop=True)

owid.sort_values(["ISO_Code", "Date"], ascending=True, inplace = True)
owid.reset_index(drop=True, inplace=True)

if EXPORT==True:
	owid.to_csv(r'../../data/preprocessed_data/owid_Parsed_Joseph.csv')