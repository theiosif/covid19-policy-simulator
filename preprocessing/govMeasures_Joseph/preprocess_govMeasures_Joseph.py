#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 20:04:00 2020

@author: yoji
"""

import pandas as pd
import re               # Regex is love

# ------- DEBUG / UTILITY FLAGS ------- #

FINAL = False # Flag to set final dataset columns/countries to be used
countries_final = []
cols_final = []

DEBUG = False  # Debug flag, to comment out blocks of done code.
              # with an "if DONE:"


NORM = True   # Debug flag, so that I can switch between norm'd
              # And un-normalized data on the fly between blocks

LOCAL = False  # get data from local csv or from web

EXPORT = True  # you get the point

# --------------------------------  DATA  ---------------------------------- #
# Legend of dataset columns:
# https://github.com/OxCGRT/covid-policy-tracker/blob/master/documentation/codebook.md
if LOCAL:
    df_oxford = pd.read_csv("OxCGRT_latest.csvOxCGRT_latest.csv")
else:
    df_oxford =  pd.read_csv("https://github.com/OxCGRT/covid-policy-tracker/raw/master/data/OxCGRT_latest.csv")

# In[]
useless = []
cols = df_oxford.columns
r = re.compile(".*Flag")
matched = list(filter(r.match, cols))

useless = matched + ["M1_Wildcard", "ConfirmedCases", "ConfirmedDeaths"]

r = re.compile(".*Legacy*")
matched = list(filter(r.match, cols))
useless += matched

r = re.compile(".*Display*")
matched = list(filter(r.match, cols))
useless += matched

# Firing off a warning shot
#print("To be dropped:" + str(useless))

df_oxford.drop(columns=useless, inplace=True)


# In[]
#print("Sum of NaNs")
#print(df_oxford.isna().sum())

#print("---> NaNs as percent per country data: <---")
nulls_percent = df_oxford.groupby('CountryName').apply(lambda x: 1 - x.notnull().mean()).drop('CountryName', axis=1)
df_oxford = df_oxford.drop(df_oxford[df_oxford.CountryName=='Gibraltar'].index)
#print(nulls_percent)

# In[]
nulls_percent = df_oxford.groupby('CountryName').apply(lambda x: 1 - x.notnull().mean()).drop('CountryName', axis=1)

# Percentage of NaNs
#print(nulls_percent)

# Top of countries which are worst at gathering numbers.
shameful_numbers = nulls_percent.sum(axis=1).sort_values(ascending=False)
#print(shameful_numbers)

cols = df_oxford.columns
shameful_columns = nulls_percent.mean(axis=0).sort_values(ascending=False)
#print(shameful_columns)

# In[]
economy = ['E1_Income support', 'E2_Debt/contract relief', 'E3_Fiscal measures', 'E4_International support', 'E1_Income support', 'E2_Debt/contract relief', 'E3_Fiscal measures', 'E4_International support']

work_cols = list(set(cols)-set(economy)-set(['Date', 'CountryCode', 'CountryName']))


def nan_fix(series):
    if series.isna().all():
        return series
    elif pd.isna(series.iloc[0]):
        series.iloc[0] = 0
        series = series.ffill().bfill()
    return series

df_oxford[work_cols] = df_oxford.groupby('CountryName')[work_cols].transform(nan_fix)


#Schhhhhh...
df_oxford[work_cols] = df_oxford.groupby('CountryName')[work_cols].ffill().bfill()
df_oxford[economy] = df_oxford[economy].fillna(value=0)

#print(df_oxford.isna().sum())

df_oxford = df_oxford.rename(columns={'CountryCode': 'ISO_Code', 'CountryName':'Country'})

df_oxford = df_oxford.drop(columns=['Country'])
df_oxford['Date'] = pd.to_datetime(df_oxford["Date"],format = "%Y%m%d")

# In[ Cut it down to intended date range]

start_date, end_date = "2020-01-01", "2020-07-31"

def correct_dates(df):
    date_range = pd.date_range(start_date , end_date)

    date_range = date_range.to_frame(index=False, name="Date")

    df = df.set_index("Date").join(date_range.set_index("Date"), how="inner")

    return df.reset_index()


df_trim = df_oxford.groupby(["ISO_Code"]).apply(correct_dates).reset_index(drop=True)


# In[Test]:
if DEBUG:
	def get_uniques(df):

	    print(len(df['Date'].unique()))

	    return df.reset_index()

	df_trim.sort_values(["ISO_Code", "Date"]).groupby(['ISO_Code']).apply(get_uniques)

# In[Done, export]
if EXPORT:
	df_trim.to_csv(r'../../data/preprocessed_data/govMeasures_Parsed_Joseph.csv')
	#df_oxford.to_csv(r'govMeasures_Parsed_Joseph.csv')

