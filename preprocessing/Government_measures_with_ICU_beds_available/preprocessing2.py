import pandas as pd


dataset = 'venv/data_preprocessed_data_Government_measures_and_ovid_1.csv'
df = pd.read_csv(dataset, sep = ",")

dataset_to_add="venv/Summary_stats_all_locs_1.csv"
df_add = pd.read_csv(dataset_to_add, sep = ",")


# change several country names to work with pycountry lib
name_dict = {'Bolivia (Plurinational State of)': 'Bolivia', 'Syrian Arab Republic': 'Syria',

             'Czechia': 'Czech Republic', 'Dominican Republic': 'Dominica',

             'Republic of Korea': 'Korea, Rep. of', 'Iran (Islamic Republic of)': 'Iran'
            }
df_add.replace({"location_name": name_dict}, inplace=True)


location_in_add = df_add['location_name'].tolist()
location_data = df['NAME'].tolist()
location_data = list(dict.fromkeys(location_data))
location_added = []
location_refused = []


# to select the available countries in both data sets
for i in location_data:
    counter = 0
    test = False
    for j in location_in_add:
        counter += 1
        tester = len(location_in_add)
        if (i == j):
            location_added.append(i)
            test = True
        if (counter == tester):
            if ( test==False):
                location_refused.append(i)


#
def getIndexes(dfObj, value):
    ''' Get index positions of value in dataframe i.e. dfObj.'''
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos
final_indexes = []

for i in location_added:
    indexes = getIndexes(df_add,i)
    final_indexes.extend(indexes)
rows_to_delete = df.index[final_indexes]
df.drop(rows_to_delete, inplace= True)
df.to_csv('venv/data_preprocessed_data_Government_measures_and_ovid_Aziz.csv')

#select the two columns from the other Data set
columns=['location_name','all_bed_capacity','icu_bed_capacity']
df_add= df_add[columns]
df_add_final = df_add.loc[final_indexes]

df_add_final.all_bed_capacity[316] = 924107
df_add_final.icu_bed_capacity[316] = 96596

# Final Merge
df['all_bed_capacity'] =00
df['icu_bed_capacity'] =00
for i in location_added:
    indexes_add = getIndexes(df_add, i)
    indexes_df = getIndexes(df, i)
    for index in indexes_df:
        df.all_bed_capacity[index] = df_add_final.all_bed_capacity[indexes_add]
        df.icu_bed_capacity[index] = df_add_final.icu_bed_capacity[indexes_add]

df.rename(columns={'NAME': 'Country','date' : 'Date','ISO': 'ISO_CODE' } , inplace=True)
df.to_csv('venv/data_preprocessed_data_Government_measures_and_ovid_Aziz.csv')
