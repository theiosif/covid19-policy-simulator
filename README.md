
# **Applied Machine Intelligence**

## group 19 - SoSe 20

---

## **Running the app**

Execute **`cd web && docker-compose up`**. The backend and frontend applications will take **a very long time to build.** Once the build process is over, navigate to **`0.0.0.0/4200`** to be greeted by the web UI.

**[!] Important note: The frontend container will throw a lot of errors, as well as even return an exit code. Do not listen to the output! If you let it build for approximately 10 minutes, it will work.**

---

## **Repository Folder Structure**

### Reports (Documentation)

* Project code is thoroughly documented through the four milestone reports.

### Data
* All datasets, cleaned datasets and the final merged dataset

### Exploration
* All code regarding exploring the datasets

### preprocessing
* All code regarding preprocessing the datasets

### Modelling
* All code regarding prediction models

### Web
* backend and frontend code

### Video
* project video presenting the idea
https://drive.google.com/file/d/1MKVugriSCKl-I4LJsLEz0VLwCzJwFZgN/view?usp=sharing

### Project Structure, as tree format:

```
├── data
│   ├── excluded from merging
│   ├── merged_data
│   │   └── FINALLL.csv
│   ├── preprocessed_data
│   └── raw_data
├── exploration
│   ├── [One folder per explored dataset, with notebook]
├── modelling
│   ├── FINAL_MODELS
│   │   ├── CO2_Emissions_Prediction
│   │   │   ├── random_forest_model_co2.pkl
│   │   │   └── RF_CO2.ipynb
│   │   ├── Covid_Cases
│   │   │   ├── random_forest_model_cases.pkl
│   │   │   └── RF_Covid_cases.ipynb
│   │   ├── FINAL_DATASET.csv
│   │   ├── GDP_Prediction
│   │   │   ├── random_forest_model_GDP.pkl
│   │   │   ├── RF_GDP.ipynb
│   │   │   └── RF_GDP_new.ipynb
│   │   └── START_END_DATE.ipynb
│   ├── LSTM
│   │   └── Model_Final.ipynb
│   ├── Model_comparator.ipynb
│   ├── Random_Forest
│   │   ├── modelling_Model_comparator__1_.ipynb
│   │   └── Random_Forest.ipynb
│   └── test
│       ├── random_forest_model_co2.pkl
│       └── RF_CO2.ipynb
├── preprocessing
│   ├── [One folder per preprocessed dataset, with notebook]
├── reports
│   ├── g19-M1.pdf
│   ├── g19-M2.pdf
│   ├── g19-M3.pdf
│   └── Milestones_Descriptions.pdf
└── web
    ├── backend
    │   ├── ami.py
    │   ├── app.py
    │   ├── Dockerfile
    ├── docker-compose.yml
    └── frontend
        ├── Dockerfile
        ├── src
        │   ├── app
        │   │   ...

```

## Contributors
Patrick Stecher  
Moritz Schüler  
Andrei-Cristian Iosif  
Aziz Raies  
Hania Wattar

---

## [dev-related (commands cheatsheet, can be safely ignored)]

### Virtualenv

Installing: `python3 -m pip install --user virtualenv`  
Creating a virtual environment activating it: `virtualenv venv && source venv/bin/activate`  
Installing necessary packages inside the venv: `pip install -r requirements.txt`  
Freezing local venv packages after adding new ones: `pip freeze -l > requirements.txt`

### git-related

Homebrew bash alias for lazy people (add it to ~/.bashrc or equiv):

```bash
function yes_or_no {
    while true; do
        read "yn?$1 [y/n]: "
        echo $yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}

git_push_all() {
    yes_or_no "Confirm?" && git add --all && git commit -m "$1" && git push
}
```

Usage: `git_push_all "[commit msg with spaces]"`

[@TODO]: <> (add usage prompt inside function)

---
